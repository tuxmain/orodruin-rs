# Changelog

## HEAD

## 0.2.0

* `sign` accepts `&[impl Into<PublicKey> + Clone]` instead of `&[PublicKey]`, allowing the use of wrappers or references
* `SecretKey::to_bytes`, `SecretKey::from_bytes`
* Specified MSRV 1.75.0
* impl `Zeroize` for `PublicKey`
* `sign` zeroizes more internal secret values
