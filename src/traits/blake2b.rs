//! Implement `Hasher` for the `blake2b_simd` crate, that does not use the `digest` crate.

use crate::traits::Hasher;

/// Blake2b hasher with both its parameters and state
#[derive(Clone, Debug)]
pub struct Blake2b<'a> {
	params: &'a blake2b_simd::Params,
	state: blake2b_simd::State,
}

/// Wrapper over a Blake2b hash
#[derive(Clone, Debug)]
pub struct Hash(pub blake2b_simd::Hash);

impl AsRef<[u8; 64]> for Hash {
	fn as_ref(&self) -> &[u8; 64] {
		self.0.as_array()
	}
}

impl core::borrow::Borrow<[u8; 64]> for Hash {
	fn borrow(&self) -> &[u8; 64] {
		self.0.as_array()
	}
}

impl<'a> Blake2b<'a> {
	/// Build a hasher with blank state from its parameters
	pub fn from_params(params: &'a blake2b_simd::Params) -> Self {
		Self {
			params,
			state: params.to_state(),
		}
	}

	/// Get the parameters
	pub fn params(&self) -> &'a blake2b_simd::Params {
		self.params
	}

	/// Get the state
	pub fn state(&self) -> &blake2b_simd::State {
		&self.state
	}

	/// Get a mutable reference to the state
	pub fn state_mut(&mut self) -> &mut blake2b_simd::State {
		&mut self.state
	}
}

impl<'a> Hasher<Hash> for Blake2b<'a> {
	fn update(&mut self, data: impl AsRef<[u8]>) {
		self.state.update(data.as_ref());
	}

	fn finalize_reset(&mut self) -> Hash {
		let hash = self.state.finalize();
		self.state = self.params.to_state();
		Hash(hash)
	}
}
