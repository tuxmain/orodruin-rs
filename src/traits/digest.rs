//! Implement `Hasher` for hashers that implement `Digest` from the `digest` crate.

use crate::traits::Hasher;

impl<H> Hasher<[u8; 64]> for H
where
	H: digest::Digest<OutputSize = digest::generic_array::typenum::U64> + digest::FixedOutputReset,
{
	fn update(&mut self, data: impl AsRef<[u8]>) {
		digest::Digest::update(self, data);
	}
	fn finalize_reset(&mut self) -> [u8; 64] {
		digest::Digest::finalize_reset(self).into()
	}
}
