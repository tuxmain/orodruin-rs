#[cfg(feature = "blake2b")]
pub mod blake2b;
#[cfg(feature = "digest")]
mod digest;

/// Something that can provide a cryptographic hash
pub trait Hasher<Hash> {
	/// Add input data
	///
	/// Can be called multiple times to concatenate more data before finalizing.
	fn update(&mut self, data: impl AsRef<[u8]>);

	/// Compute the final hash and reset the state
	///
	/// Returns the hash corresponding to all the data passed to `update` since the previous call to this method.
	/// Re-using the hasher after this method is called should behave as if a new hasher was created,
	/// except it should hopefully be more performant.
	fn finalize_reset(&mut self) -> Hash;
}
