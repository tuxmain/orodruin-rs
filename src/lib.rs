#![no_std]
#![warn(missing_docs)]
#![deny(non_ascii_idents)]
#![deny(unnameable_types)]
#![deny(unreachable_pub)]
#![deny(unsafe_code)]
#![deny(unstable_features)]
#![warn(unused_qualifications)]
#![allow(clippy::tabs_in_doc_comments)]

//! # Orodruin
//!
//! Safe Rust Monero-like bLSAG ring signatures on Ristretto curve, as described in [Zero to Monero](https://www.getmonero.org/library/Zero-to-Monero-2-0-0.pdf).
//!
//! ## Ring signature
//!
//! A "ring" is a set of public keys. A ring signature is a signature that can only be forged by a member of the ring, and while you can verify its authenticity, you cannot identify which member forged it.
//!
//! This crate implements bLSAG, a kind of ring signature where a different signatures issued by the same key can be proved to be linked together (without breaking the anonymity). You may want to use single-use keys to avoid linkability.
//!
//! Typical applications are anonymous voting systems and anonymous transactions (e.g. Monero).
//!
//! ## Features
//!
//! * Sign (feature `alloc`)
//! * Verify (no-std)
//! * Generic for cryptographic 512-bit hashers that impl `digest::Digest` (e.g. sha2) (feature `digest`)
//! * Impl for `blake2b-simd` (feature `blake2b`)
//! * You can easily impl other hashers
//! * **No** guarantee about constant-time operation.
//! * **No** guarantee about side-channel attack mitigation.
//! * MSRV 1.75.0
//! 
//! Secret key, public key, key image types are 32 bytes each. Signature for a ring of N public keys is at least 64+32N bytes.
//!
//! ## Example
//!
//! ```
//! use orodruin::*;
//!
//! let mut rng = rand::thread_rng();
//! let mut hasher = sha2::Sha512::default();
//!
//! let secret_keys: Vec<SecretKey> = (0..4).map(|_| SecretKey::random(&mut rng)).collect();
//! let ring: Vec<PublicKey> = secret_keys.iter().map(SecretKey::public_key).collect();
//!
//! for (i, secret_key) in secret_keys.into_iter().enumerate() {
//! 	let message = i.to_be_bytes();
//! 	let signature = sign(secret_key, &ring, i, &message, &mut rng, &mut hasher);
//! 	assert_eq!(signature.verify(ring.iter(), &message, &mut hasher), Ok(()));
//! }
//! ```
//! 
//! You can check whether two valid signatures have been signed by the same secret key by checking whether their `.key_image` are equal.

mod traits;

#[cfg(feature = "blake2b")]
pub use traits::blake2b;
pub use traits::Hasher;

#[cfg(feature = "alloc")]
extern crate alloc;

#[cfg(feature = "alloc")]
use alloc::vec::Vec;
use core::borrow::Borrow;
#[cfg(feature = "alloc")]
use curve25519_dalek::traits::MultiscalarMul;
use curve25519_dalek::{
	constants,
	ristretto::{CompressedRistretto, RistrettoPoint},
	scalar::Scalar,
	traits::IsIdentity,
};
#[cfg(feature = "alloc")]
use rand_core::{CryptoRng, RngCore};
#[cfg(feature = "zeroize")]
use zeroize::{Zeroize, ZeroizeOnDrop};

/// Ring secret key
#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub struct SecretKey(pub Scalar);

#[cfg(feature = "zeroize")]
impl Zeroize for SecretKey {
	fn zeroize(&mut self) {
		self.0.zeroize()
	}
}
#[cfg(feature = "zeroize")]
impl ZeroizeOnDrop for SecretKey {}

impl SecretKey {
	/// Derive the public key from the private key
	pub fn public_key(&self) -> PublicKey {
		PublicKey((self.0 * constants::RISTRETTO_BASEPOINT_POINT).compress())
	}

	/// Generate a random private key
	#[cfg(feature = "alloc")]
	pub fn random(mut rng: impl CryptoRng + RngCore) -> Self {
		Self(Scalar::random(&mut rng))
	}

	/// Derive the key image from the private key
	pub fn key_image<H: Hasher<impl Borrow<[u8; 64]>>>(&self, hasher: &mut H) -> KeyImage {
		Hasher::update(hasher, self.public_key().0.as_bytes());
		let pk_hash = ristretto_point_from_hash(hasher);
		KeyImage((self.0 * pk_hash).compress())
	}

	/// Underlying byte representation
	pub fn to_bytes(self) -> [u8; 32] {
		self.0.to_bytes()
	}

	/// Transform bytes into a secret key
	///
	/// Because the given bytes may not be a canonical representation, this function is not reciprocal to `to_bytes`.
	pub fn from_bytes(bytes: [u8; 32]) -> Self {
		Self(Scalar::from_bytes_mod_order(bytes))
	}
}

impl From<SecretKey> for Scalar {
	fn from(secret_key: SecretKey) -> Scalar {
		secret_key.0
	}
}

impl AsRef<Scalar> for SecretKey {
	fn as_ref(&self) -> &Scalar {
		&self.0
	}
}

/// Ring public key
#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub struct PublicKey(pub CompressedRistretto);

#[cfg(feature = "zeroize")]
impl Zeroize for PublicKey {
	fn zeroize(&mut self) {
		self.0.zeroize()
	}
}

impl PublicKey {
	/// Derive the key image from the private key (more efficiently than `SecretKey::key_image`)
	/// 
	/// `sk` should be the secret key associated to the public key `self`.
	/// This is not checked by this function.
	pub fn key_image<H: Hasher<impl Borrow<[u8; 64]>>>(
		&self,
		sk: &SecretKey,
		hasher: &mut H,
	) -> KeyImage {
		Hasher::update(hasher, self.0.as_bytes());
		let pk_hash = ristretto_point_from_hash(hasher);
		KeyImage((sk.0 * pk_hash).compress())
	}
}

impl From<SecretKey> for PublicKey {
	fn from(secret_key: SecretKey) -> Self {
		secret_key.public_key()
	}
}

impl From<&SecretKey> for PublicKey {
	fn from(secret_key: &SecretKey) -> Self {
		secret_key.public_key()
	}
}

/// Deterministic image of the private key
#[derive(Clone, Debug, Eq, Hash, PartialEq)]
pub struct KeyImage(pub CompressedRistretto);

/// Ring signature
///
/// Normal usage will not need to access the fields directly.
/// However this can be useful in case compatibility glue is needed.
///
/// Size: O(N) with N members in the ring
#[derive(Clone, Debug, PartialEq)]
pub struct Signature<V> {
	/// bLSAG challenge
	pub challenge: Scalar,

	/// Deterministic image of the private key
	///
	/// Can be used to ensure a private key did not sign multiple times.
	/// An efficient way to do this (in O(N log N)) is to use key images as keys in a hash map.
	pub key_image: KeyImage,

	/// bLSAG responses
	pub responses: V,
}

impl<V: AsRef<[Scalar]>> Verifiable for Signature<V> {
	fn challenge(&self) -> Scalar {
		self.challenge
	}
	fn key_image(&self) -> KeyImage {
		self.key_image.clone()
	}
	fn responses(&self) -> impl AsRef<[Scalar]> {
		&self.responses
	}
}

/// A ring signature that can be verified
///
/// You only need to implement the 3 getters to make your own signature type.
pub trait Verifiable {
	/// bLSAG challenge
	fn challenge(&self) -> Scalar;

	/// Deterministic image of the private key
	///
	/// Can be used to ensure a private key did not sign multiple times.
	/// An efficient way to do this (in O(N log N)) is to use key images as keys in a hash map.
	fn key_image(&self) -> KeyImage;

	/// bLSAG responses
	fn responses(&self) -> impl AsRef<[Scalar]>;

	/// Get the number of public keys in signature's ring
	fn get_ring_len(&self) -> usize {
		self.responses().as_ref().len()
	}

	/// Verify whether the signature is valid, i.e. its signer is in the ring
	fn verify<H: Hasher<impl Borrow<[u8; 64]>>>(
		&self,
		ring: impl ExactSizeIterator<Item = impl Borrow<PublicKey>>,
		message: &[u8],
		hasher: &mut H,
	) -> Result<(), VerifyError> {
		if ring.len() != self.responses().as_ref().len() {
			return Err(VerifyError::BadRingSize);
		}
		let key_image = self
			.key_image()
			.0
			.decompress()
			.ok_or(VerifyError::CannotDecompressKeyImage)?;
		// Check the key image's order as recommended.
		// It may not be necessary, as decompression may fail before, but let's play safe.
		#[allow(deprecated)]
		if !(constants::BASEPOINT_ORDER * key_image).is_identity() {
			return Err(VerifyError::BadKeyImageOrder);
		}
		let mut cp = self.challenge();
		for (pki, ri) in ring.zip(self.responses().as_ref().iter()) {
			Hasher::update(hasher, pki.borrow().0.as_bytes());
			let pki_hash = ristretto_point_from_hash(hasher);
			Hasher::update(hasher, message);
			Hasher::update(
				hasher,
				(ri * constants::RISTRETTO_BASEPOINT_POINT
					+ cp * pki
						.borrow()
						.0
						.decompress()
						.ok_or(VerifyError::CannotDecompressRingKey)?)
				.compress()
				.as_bytes(),
			);
			Hasher::update(
				hasher,
				(ri * pki_hash + cp * key_image).compress().as_bytes(),
			);
			cp = scalar_from_hash(hasher);
		}
		if self.challenge() == cp {
			Ok(())
		} else {
			Err(VerifyError::BadChallenge)
		}
	}
}

fn ristretto_point_from_hash<H: Hasher<impl Borrow<[u8; 64]>>>(hasher: &mut H) -> RistrettoPoint {
	RistrettoPoint::from_uniform_bytes(hasher.finalize_reset().borrow())
}

fn scalar_from_hash<H: Hasher<impl Borrow<[u8; 64]>>>(hasher: &mut H) -> Scalar {
	Scalar::from_bytes_mod_order_wide(hasher.finalize_reset().borrow())
}

/// Assumes `ring[secret_index]` is `sk`'s public key
#[cfg(feature = "alloc")]
pub fn sign<H: Hasher<impl Borrow<[u8; 64]>>>(
	sk: SecretKey,
	ring: &[impl Into<PublicKey> + Clone],
	secret_index: usize,
	message: &[u8],
	mut rng: impl CryptoRng + RngCore,
	hasher: &mut H,
) -> Signature<Vec<Scalar>> {
	assert!(!ring.is_empty());
	let n = ring.len();
	let pk = Into::<PublicKey>::into(ring[secret_index].clone()).0;
	Hasher::update(hasher, pk.as_bytes());
	let pk_hash = ristretto_point_from_hash(hasher);
	let image: RistrettoPoint = sk.0 * pk_hash;

	let mut r: Vec<Scalar> = (0..n).map(|_| Scalar::random(&mut rng)).collect();
	let a = r[secret_index];
	Hasher::update(hasher, message);
	Hasher::update(
		hasher,
		(a * constants::RISTRETTO_BASEPOINT_POINT)
			.compress()
			.as_bytes(),
	);
	Hasher::update(hasher, (a * pk_hash).compress().as_bytes());
	let mut cp = scalar_from_hash(hasher);
	let mut c0 = cp;
	let (r1, r2) = r.split_at(secret_index);
	let (pk1, pk2) = ring.split_at(secret_index);
	for (i, (ri, pki)) in r2
		.iter()
		.zip(pk2.iter())
		.enumerate()
		.skip(1)
		.chain(r1.iter().zip(pk1.iter()).enumerate())
	{
		if i == 0 {
			c0 = cp;
		}
		Hasher::update(hasher, Into::<PublicKey>::into(pki.clone()).0.as_bytes());
		let pki_hash = ristretto_point_from_hash(hasher);
		Hasher::update(hasher, message);
		Hasher::update(
			hasher,
			(RistrettoPoint::multiscalar_mul(
				&[*ri, cp],
				&[
					constants::RISTRETTO_BASEPOINT_POINT,
					Into::<PublicKey>::into(pki.clone())
						.0
						.decompress()
						.expect("Cannot decompress key in ring"),
				],
			))
			.compress()
			.as_bytes(),
		);
		Hasher::update(
			hasher,
			(RistrettoPoint::multiscalar_mul(&[*ri, cp], &[pki_hash, image]))
				.compress()
				.as_bytes(),
		);
		cp = scalar_from_hash(hasher);
		if i == n - 1 {
			c0 = cp;
		}
	}
	r[secret_index] = a - cp * sk.0;

	#[cfg(feature = "zeroize")]
	{
		let mut secret_index = secret_index;
		secret_index.zeroize();
		let mut a = a;
		a.zeroize();
		let mut sk = sk;
		sk.zeroize();
		cp.zeroize();
	}

	Signature {
		challenge: c0,
		key_image: KeyImage(image.compress()),
		responses: r,
	}
}

/// Error giving the reason why a signature is invalid
#[derive(Clone, Debug, Eq, PartialEq)]
pub enum VerifyError {
	/// Invalid signature, possibly because the signer is not in the ring
	BadChallenge,
	/// Invalid signature, probably because of an attack attempt
	BadKeyImageOrder,
	/// The number of responses is not equal to the ring size
	BadRingSize,
	/// Invalid compressed key image
	CannotDecompressKeyImage,
	/// Invalid key in the ring
	CannotDecompressRingKey,
}

/// Test whether two signatures were emitted by the same private key
pub fn signatures_are_linked<V>(signature_1: &Signature<V>, signature_2: &Signature<V>) -> bool {
	signature_1.key_image == signature_2.key_image
}

#[cfg(test)]
#[macro_use]
extern crate std;

#[cfg(test)]
mod test {
	use rand::SeedableRng;

	use super::*;

	/// A valid signature should be verified correctly.
	fn correctness<H: Hasher<T>, T: Borrow<[u8; 64]>>(mut hasher: H) {
		let n = 4;
		let mut rng = rand::rngs::StdRng::seed_from_u64(3468749987268879478);

		let sks: Vec<SecretKey> = (0..n).map(|_| SecretKey::random(&mut rng)).collect();
		let ring: Vec<PublicKey> = sks.iter().map(SecretKey::public_key).collect();

		for (i, (ski, _pki)) in sks.iter().zip(ring.iter()).enumerate() {
			let message = i.to_be_bytes();
			let signature = sign(ski.clone(), &ring, i, &message, &mut rng, &mut hasher);
			assert_eq!(signature.verify(ring.iter(), &message, &mut hasher), Ok(()));
		}
	}

	/// A valid signature for a strict subset of the ring should fail the verification.
	fn fail_strict_subset<H: Hasher<T>, T: Borrow<[u8; 64]>>(mut hasher: H) {
		let n = 4;
		let mut rng = rand::rngs::StdRng::seed_from_u64(3468749987268879478);

		let sks: Vec<SecretKey> = (0..n).map(|_| SecretKey::random(&mut rng)).collect();
		let ring: Vec<PublicKey> = sks.iter().map(SecretKey::public_key).collect();

		for (i, (ski, _pki)) in sks.iter().zip(ring.iter()).enumerate() {
			let message = i.to_be_bytes();
			let signature = sign(ski.clone(), &ring, i, &message, &mut rng, &mut hasher);
			assert_eq!(
				signature.verify(ring[0..n - 1].iter(), &message, &mut hasher),
				Err(VerifyError::BadRingSize)
			);
		}
	}

	/// The wrong message should invalidate the signature.
	fn fail_wrong_message<H: Hasher<T>, T: Borrow<[u8; 64]>>(mut hasher: H) {
		let n = 4;
		let mut rng = rand::rngs::StdRng::seed_from_u64(3468749987268879478);

		let sks: Vec<SecretKey> = (0..n).map(|_| SecretKey::random(&mut rng)).collect();
		let ring: Vec<PublicKey> = sks.iter().map(SecretKey::public_key).collect();

		for (i, (ski, _pki)) in sks.iter().zip(ring.iter()).enumerate() {
			let message = i.to_be_bytes();
			let signature = sign(ski.clone(), &ring, i, &message, &mut rng, &mut hasher);
			let wrong_message = (i + 1).to_be_bytes();
			assert_eq!(
				signature.verify(ring.iter(), &wrong_message, &mut hasher),
				Err(VerifyError::BadChallenge)
			);
		}
	}

	/// The wrong key image should invalidate the signature.
	fn fail_wrong_key_image<H: Hasher<T>, T: Borrow<[u8; 64]>>(mut hasher: H) {
		let n = 4;
		let mut rng = rand::rngs::StdRng::seed_from_u64(3468749987268879478);

		let sks: Vec<SecretKey> = (0..n).map(|_| SecretKey::random(&mut rng)).collect();
		let ring: Vec<PublicKey> = sks.iter().map(SecretKey::public_key).collect();

		for (i, (ski, _pki)) in sks.iter().zip(ring.iter()).enumerate() {
			let message = i.to_be_bytes();
			let mut signature = sign(ski.clone(), &ring, i, &message, &mut rng, &mut hasher);
			signature.key_image.0 =
				(signature.key_image.0.decompress().unwrap() * Scalar::from(2_u32)).compress();
			assert_eq!(
				signature.verify(ring.iter(), &message, &mut hasher),
				Err(VerifyError::BadChallenge)
			);
		}
	}

	/// Any wrong response should invalidate the signature.
	fn fail_wrong_response<H: Hasher<T>, T: Borrow<[u8; 64]>>(mut hasher: H) {
		let n = 4;
		let mut rng = rand::rngs::StdRng::seed_from_u64(3468749987268879478);

		let sks: Vec<SecretKey> = (0..n).map(|_| SecretKey::random(&mut rng)).collect();
		let ring: Vec<PublicKey> = sks.iter().map(SecretKey::public_key).collect();

		for (i, (ski, _pki)) in sks.iter().zip(ring.iter()).enumerate() {
			let message = i.to_be_bytes();
			let signature = sign(ski.clone(), &ring, i, &message, &mut rng, &mut hasher);
			for j in 0..n {
				let mut signature = signature.clone();
				signature.responses[j] *= Scalar::from(2_u32);
				assert_eq!(
					signature.verify(ring.iter(), &message, &mut hasher),
					Err(VerifyError::BadChallenge)
				);
			}
		}
	}

	/// Any wrong pubkey should invalidate the signature.
	fn fail_wrong_ring<H: Hasher<T>, T: Borrow<[u8; 64]>>(mut hasher: H) {
		let n = 4;
		let mut rng = rand::rngs::StdRng::seed_from_u64(3468749987268879478);

		let sks: Vec<SecretKey> = (0..n).map(|_| SecretKey::random(&mut rng)).collect();
		let ring: Vec<PublicKey> = sks.iter().map(SecretKey::public_key).collect();

		for (i, (ski, _pki)) in sks.iter().zip(ring.iter()).enumerate() {
			let message = i.to_be_bytes();
			let signature = sign(ski.clone(), &ring, i, &message, &mut rng, &mut hasher);
			for j in 0..n {
				assert_eq!(
					signature.verify(
						ring.iter().enumerate().map(|(k, pk)| if k == j {
							PublicKey((pk.0.decompress().unwrap() * Scalar::from(2_u32)).compress())
						} else {
							pk.clone()
						}),
						&message,
						&mut hasher
					),
					Err(VerifyError::BadChallenge)
				);
			}
		}
	}

	#[cfg(feature = "digest")]
	#[test]
	fn correctness_sha2() {
		correctness(sha2::Sha512::default());
	}

	#[cfg(feature = "digest")]
	#[test]
	fn fail_strict_subset_sha2() {
		fail_strict_subset(sha2::Sha512::default());
	}

	#[cfg(feature = "digest")]
	#[test]
	fn fail_wrong_message_sha2() {
		fail_wrong_message(sha2::Sha512::default());
	}

	#[cfg(feature = "digest")]
	#[test]
	fn fail_wrong_key_image_sha2() {
		fail_wrong_key_image(sha2::Sha512::default());
	}

	#[cfg(feature = "digest")]
	#[test]
	fn fail_wrong_response_sha2() {
		fail_wrong_response(sha2::Sha512::default());
	}

	#[cfg(feature = "digest")]
	#[test]
	fn fail_wrong_ring_sha2() {
		fail_wrong_ring(sha2::Sha512::default());
	}

	#[cfg(feature = "blake2b")]
	#[test]
	fn correctness_blake2b() {
		let mut params = blake2b_simd::Params::new();
		params.hash_length(64);
		correctness(blake2b::Blake2b::from_params(&params));
	}

	#[cfg(feature = "blake2b")]
	#[test]
	fn fail_strict_subset_blake2b() {
		let mut params = blake2b_simd::Params::new();
		params.hash_length(64);
		fail_strict_subset(blake2b::Blake2b::from_params(&params));
	}

	#[cfg(feature = "blake2b")]
	#[test]
	fn fail_wrong_message_blake2b() {
		let mut params = blake2b_simd::Params::new();
		params.hash_length(64);
		fail_wrong_message(blake2b::Blake2b::from_params(&params));
	}

	#[cfg(feature = "blake2b")]
	#[test]
	fn fail_wrong_key_image_blake2b() {
		let mut params = blake2b_simd::Params::new();
		params.hash_length(64);
		fail_wrong_key_image(blake2b::Blake2b::from_params(&params));
	}

	#[cfg(feature = "blake2b")]
	#[test]
	fn fail_wrong_response_blake2b() {
		let mut params = blake2b_simd::Params::new();
		params.hash_length(64);
		fail_wrong_response(blake2b::Blake2b::from_params(&params));
	}

	#[cfg(feature = "blake2b")]
	#[test]
	fn fail_wrong_ring_blake2b() {
		let mut params = blake2b_simd::Params::new();
		params.hash_length(64);
		fail_wrong_ring(blake2b::Blake2b::from_params(&params));
	}
}
